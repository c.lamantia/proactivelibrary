/**
 */
package editautomaton.impl;

import editautomaton.EditAutomaton;
import editautomaton.EditautomatonPackage;
import editautomaton.State;
import editautomaton.Transition;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Edit Automaton</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link editautomaton.impl.EditAutomatonImpl#getStates <em>States</em>}</li>
 *   <li>{@link editautomaton.impl.EditAutomatonImpl#getTransitions <em>Transitions</em>}</li>
 *   <li>{@link editautomaton.impl.EditAutomatonImpl#getName <em>Name</em>}</li>
 *   <li>{@link editautomaton.impl.EditAutomatonImpl#getHandledObject <em>Handled Object</em>}</li>
 *   <li>{@link editautomaton.impl.EditAutomatonImpl#getApi <em>Api</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EditAutomatonImpl extends MinimalEObjectImpl.Container implements EditAutomaton {
	/**
	 * The cached value of the '{@link #getStates() <em>States</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStates()
	 * @generated
	 * @ordered
	 */
	protected EList<State> states;

	/**
	 * The cached value of the '{@link #getTransitions() <em>Transitions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<Transition> transitions;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getHandledObject() <em>Handled Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHandledObject()
	 * @generated
	 * @ordered
	 */
	protected static final String HANDLED_OBJECT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHandledObject() <em>Handled Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHandledObject()
	 * @generated
	 * @ordered
	 */
	protected String handledObject = HANDLED_OBJECT_EDEFAULT;

	/**
	 * The default value of the '{@link #getApi() <em>Api</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApi()
	 * @generated
	 * @ordered
	 */
	protected static final String API_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getApi() <em>Api</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApi()
	 * @generated
	 * @ordered
	 */
	protected String api = API_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EditAutomatonImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EditautomatonPackage.Literals.EDIT_AUTOMATON;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<State> getStates() {
		if (states == null) {
			states = new EObjectContainmentEList<State>(State.class, this, EditautomatonPackage.EDIT_AUTOMATON__STATES);
		}
		return states;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transition> getTransitions() {
		if (transitions == null) {
			transitions = new EObjectContainmentEList<Transition>(Transition.class, this, EditautomatonPackage.EDIT_AUTOMATON__TRANSITIONS);
		}
		return transitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EditautomatonPackage.EDIT_AUTOMATON__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHandledObject() {
		return handledObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHandledObject(String newHandledObject) {
		String oldHandledObject = handledObject;
		handledObject = newHandledObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EditautomatonPackage.EDIT_AUTOMATON__HANDLED_OBJECT, oldHandledObject, handledObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getApi() {
		return api;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApi(String newApi) {
		String oldApi = api;
		api = newApi;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, EditautomatonPackage.EDIT_AUTOMATON__API, oldApi, api));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case EditautomatonPackage.EDIT_AUTOMATON__STATES:
				return ((InternalEList<?>)getStates()).basicRemove(otherEnd, msgs);
			case EditautomatonPackage.EDIT_AUTOMATON__TRANSITIONS:
				return ((InternalEList<?>)getTransitions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case EditautomatonPackage.EDIT_AUTOMATON__STATES:
				return getStates();
			case EditautomatonPackage.EDIT_AUTOMATON__TRANSITIONS:
				return getTransitions();
			case EditautomatonPackage.EDIT_AUTOMATON__NAME:
				return getName();
			case EditautomatonPackage.EDIT_AUTOMATON__HANDLED_OBJECT:
				return getHandledObject();
			case EditautomatonPackage.EDIT_AUTOMATON__API:
				return getApi();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case EditautomatonPackage.EDIT_AUTOMATON__STATES:
				getStates().clear();
				getStates().addAll((Collection<? extends State>)newValue);
				return;
			case EditautomatonPackage.EDIT_AUTOMATON__TRANSITIONS:
				getTransitions().clear();
				getTransitions().addAll((Collection<? extends Transition>)newValue);
				return;
			case EditautomatonPackage.EDIT_AUTOMATON__NAME:
				setName((String)newValue);
				return;
			case EditautomatonPackage.EDIT_AUTOMATON__HANDLED_OBJECT:
				setHandledObject((String)newValue);
				return;
			case EditautomatonPackage.EDIT_AUTOMATON__API:
				setApi((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case EditautomatonPackage.EDIT_AUTOMATON__STATES:
				getStates().clear();
				return;
			case EditautomatonPackage.EDIT_AUTOMATON__TRANSITIONS:
				getTransitions().clear();
				return;
			case EditautomatonPackage.EDIT_AUTOMATON__NAME:
				setName(NAME_EDEFAULT);
				return;
			case EditautomatonPackage.EDIT_AUTOMATON__HANDLED_OBJECT:
				setHandledObject(HANDLED_OBJECT_EDEFAULT);
				return;
			case EditautomatonPackage.EDIT_AUTOMATON__API:
				setApi(API_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case EditautomatonPackage.EDIT_AUTOMATON__STATES:
				return states != null && !states.isEmpty();
			case EditautomatonPackage.EDIT_AUTOMATON__TRANSITIONS:
				return transitions != null && !transitions.isEmpty();
			case EditautomatonPackage.EDIT_AUTOMATON__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case EditautomatonPackage.EDIT_AUTOMATON__HANDLED_OBJECT:
				return HANDLED_OBJECT_EDEFAULT == null ? handledObject != null : !HANDLED_OBJECT_EDEFAULT.equals(handledObject);
			case EditautomatonPackage.EDIT_AUTOMATON__API:
				return API_EDEFAULT == null ? api != null : !API_EDEFAULT.equals(api);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", handledObject: ");
		result.append(handledObject);
		result.append(", api: ");
		result.append(api);
		result.append(')');
		return result.toString();
	}

} //EditAutomatonImpl
