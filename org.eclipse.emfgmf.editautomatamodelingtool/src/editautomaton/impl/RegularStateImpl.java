/**
 */
package editautomaton.impl;

import editautomaton.EditautomatonPackage;
import editautomaton.RegularState;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Regular State</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RegularStateImpl extends StateImpl implements RegularState {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RegularStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EditautomatonPackage.Literals.REGULAR_STATE;
	}

} //RegularStateImpl
