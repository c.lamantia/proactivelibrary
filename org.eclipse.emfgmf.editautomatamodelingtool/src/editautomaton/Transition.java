/**
 */
package editautomaton;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link editautomaton.Transition#getInterceptedAction <em>Intercepted Action</em>}</li>
 *   <li>{@link editautomaton.Transition#getActionToPerform <em>Action To Perform</em>}</li>
 *   <li>{@link editautomaton.Transition#getSource <em>Source</em>}</li>
 *   <li>{@link editautomaton.Transition#getTarget <em>Target</em>}</li>
 * </ul>
 *
 * @see editautomaton.EditautomatonPackage#getTransition()
 * @model
 * @generated
 */
public interface Transition extends EObject {
	/**
	 * Returns the value of the '<em><b>Intercepted Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Intercepted Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Intercepted Action</em>' attribute.
	 * @see #setInterceptedAction(String)
	 * @see editautomaton.EditautomatonPackage#getTransition_InterceptedAction()
	 * @model dataType="editautomaton.ActionSignature"
	 * @generated
	 */
	String getInterceptedAction();

	/**
	 * Sets the value of the '{@link editautomaton.Transition#getInterceptedAction <em>Intercepted Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Intercepted Action</em>' attribute.
	 * @see #getInterceptedAction()
	 * @generated
	 */
	void setInterceptedAction(String value);

	/**
	 * Returns the value of the '<em><b>Action To Perform</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action To Perform</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action To Perform</em>' attribute.
	 * @see #setActionToPerform(String)
	 * @see editautomaton.EditautomatonPackage#getTransition_ActionToPerform()
	 * @model dataType="editautomaton.ActionSequence"
	 * @generated
	 */
	String getActionToPerform();

	/**
	 * Sets the value of the '{@link editautomaton.Transition#getActionToPerform <em>Action To Perform</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action To Perform</em>' attribute.
	 * @see #getActionToPerform()
	 * @generated
	 */
	void setActionToPerform(String value);

	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link editautomaton.State#getOutgoingTransitions <em>Outgoing Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(State)
	 * @see editautomaton.EditautomatonPackage#getTransition_Source()
	 * @see editautomaton.State#getOutgoingTransitions
	 * @model opposite="outgoingTransitions" required="true"
	 * @generated
	 */
	State getSource();

	/**
	 * Sets the value of the '{@link editautomaton.Transition#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(State value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link editautomaton.State#getIncomingTransitions <em>Incoming Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(State)
	 * @see editautomaton.EditautomatonPackage#getTransition_Target()
	 * @see editautomaton.State#getIncomingTransitions
	 * @model opposite="incomingTransitions" required="true"
	 * @generated
	 */
	State getTarget();

	/**
	 * Sets the value of the '{@link editautomaton.Transition#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(State value);

} // Transition
