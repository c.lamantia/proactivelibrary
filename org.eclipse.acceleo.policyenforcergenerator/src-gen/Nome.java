package oscar.emptyenforcer;
import static de.robv.android.xposed.XposedHelpers.findAndHookMethod;
import static de.robv.android.xposed.XposedBridge.hookAllMethods;
import static de.robv.android.xposed.XposedBridge.hookAllConstructors;
import static de.robv.android.xposed.XposedHelpers.findClass;

import static de.robv.android.xposed.XposedHelpers.findAndHookConstructor;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;

import dalvik.system.DexFile;
import android.os.IInterface;
import android.os.PowerManager;
import android.os.WorkSource;


public class EmptyEnforcer  implements IXposedHookLoadPackage
{
	private final int S0 = 1;
	private final int S1 = 2;
	private final int S2 = 3;
	private final int S3 = 4;
	
	private PowerManager.WakeLock instance = null;
	
	
	
	private boolean doNotAlterateExecution = false;

	private HashMap<String, Integer> currentStates = new HashMap<String, Integer>();
	private HashMap<String, android.os.PowerManager.WakeLock > lifeCycleObject2resource = new HashMap<String, android.os.PowerManager.WakeLock>();
	private HashMap<String, Boolean > method2hooked = new HashMap<String, Boolean>();
	private HashMap<String, String > lifeCycleObject2class = new HashMap<String, String>();

	private String mCurrentLifeCycleObject;
		private Activity mCurrentLifeCycleObjectOBJ;
	
	private String lifeCycleObjectClassString = "android.app.Activity";
	private String handledObjectClassString = "android.os.PowerManager.WakeLock";
	public final ArrayList<String> classList = new ArrayList<>();
	
	private ArrayList<String> getClassesOfPackage(String packageName, String apk) {
	        ArrayList<String> classes = new ArrayList<String>();
	        try {
	            DexFile df = new DexFile(apk);
	            for (Enumeration<String> iter = df.entries(); iter.hasMoreElements(); ) {
	                String className = iter.nextElement();
	                if (!className.contains("android")) {
	                    classes.add(className);
	                }
	            }
	        } catch (IOException e) {
	        }
	
	        return classes;
	}

	@Override
	  public void handleLoadPackage(XC_LoadPackage.LoadPackageParam lpparam) throws Throwable
	  {
		if(!lpparam.packageName.contains("gps")) return;
		final Class<?> lifeCycleObjectClass = findClass(lifeCycleObjectClassString, lpparam.classLoader);
		final Class<?> handledObjectClass = findClass(handledObjectClassString, lpparam.classLoader);
	
			ArrayList<String> clazz = getClassesOfPackage(lpparam.packageName, lpparam.appInfo.sourceDir);
			clazz.add("android.app.Activity");
			clazz.add("android.os.PowerManager.WakeLock");
	
	

		Class<?> instrumentation = XposedHelpers.findClass("android.app.Instrumentation", lpparam.classLoader);
	
		        hookAllMethods(instrumentation, "newActivity", new XC_MethodHook() {
	
	            @Override
	            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
	
	                Activity mCurrentLifeCycleObjectOBJ = (Activity) param.getResult();
	                mCurrentLifeCycleObject = mCurrentLifeCycleObjectOBJ.getClass().getCanonicalName();
	
	                if (!currentStates.containsKey(mCurrentLifeCycleObject)) {
	                    currentStates.put(mCurrentLifeCycleObject, S0);
	                }
	
	            }
	        });
	
	       
        if (lifeCycleObjectClass == null) return;
		if (handledObjectClass == null) return;
	

	for (int i = 0; i < clazz.size(); i++) {
            if (clazz.get(i).contains("$")) continue;
            Class targetClass = null;
            try {
                targetClass = findClass(clazz.get(i), lpparam.classLoader);
            } catch (Exception e) {
                //e.printStackTrace();
            }catch (Error e){
                //e.printStackTrace();
            }
   
            boolean isTargetLifeCycleObject = false;
            try {
                isTargetLifeCycleObject = lifeCycleObjectClass.isAssignableFrom(targetClass);
				if(isTargetLifeCycleObject){
					XposedBridge.log("OK" + "lifeCycleObjectClazz: " + lifeCycleObjectClass + "//////" + "targetClass: " + targetClass);
					classList.add(targetClass.toString().replace("class ", ""));
				}
            } catch (Exception e) {
                //e.printStackTrace();
            } catch (Error e){
				
			}
            if (isTargetLifeCycleObject) {


				try{
						
					findAndHookMethod(targetClass, "onResume", new XC_MethodHook(){
				
				
					
					public android.os.PowerManager.WakeLock resource = null;
					@SuppressLint("MissingPermission")
					@Override
					protected void beforeHookedMethod(MethodHookParam param) throws Throwable
					{		
									android.app.Activity lifeCycleObjectOBJ = (android.app.Activity)param.thisObject;
									String lifeCycleObject = lifeCycleObjectOBJ.getClass().getCanonicalName();
							
							String idMappa = lifeCycleObject + "after#android.app.Activity().onResume()";
							XposedBridge.log("BEFORE###after#android.app.Activity().onResume()" + lifeCycleObject);
							if (!doNotAlterateExecution){	
								super.beforeHookedMethod(param);
								if ((method2hooked.get("after#android.app.Activity().onResume()")==null)||!(method2hooked.get("after#android.app.Activity().onResume()"))){
					            	method2hooked.put("after#android.app.Activity().onResume()", true);
									lifeCycleObject2class.put(idMappa, param.method.getDeclaringClass().toString());
				
				
								}
							}		}
							
						
				
				
					@SuppressLint("MissingPermission")	
					@Override
					protected void afterHookedMethod(MethodHookParam param) throws Throwable
					{	
									android.app.Activity lifeCycleObjectOBJ = (android.app.Activity)param.thisObject;
									String lifeCycleObject = lifeCycleObjectOBJ.getClass().getCanonicalName();
						
						String idMappa = lifeCycleObject + "after#android.app.Activity().onResume()";
						XposedBridge.log("AFTER###after#android.app.Activity().onResume()" + lifeCycleObject);
						if (!doNotAlterateExecution)
						{	
							super.afterHookedMethod(param);
							if(method2hooked.get("after#android.app.Activity().onResume()")) {
								XposedBridge.log("lifeCycleObject2class: " + lifeCycleObject2class.get(idMappa) + "/// param: " + param.method.getDeclaringClass().toString());
								if(lifeCycleObject2class.get(idMappa)!= null && lifeCycleObject2class.get(idMappa).equals(param.method.getDeclaringClass().toString())){
									method2hooked.put("after#android.app.Activity().onResume()", false);
									lifeCycleObject2class.put(idMappa, "");
				
				
									if(currentStates.get(lifeCycleObject)!= null && currentStates.get(lifeCycleObject) == S0)         
									{
				
										XposedBridge.log("cambio di stato AFTER, lifeCycleObject = " + lifeCycleObject +" da S0 a S1, transition: editautomaton.impl.TransitionImpl@1040be71 (interceptedAction: after#android.app.Activity().onResume(), actionToPerform: after#android.app.Activity().onResume())");
										currentStates.put(lifeCycleObject,S1);
									}
									else if (currentStates.get(lifeCycleObject)!= null && currentStates.get(lifeCycleObject) == S3)
									{
								XposedBridge.log("Enforcer");
											if(lifeCycleObject2resource.get(lifeCycleObject)!=null){
												            lifeCycleObject2resource.get(lifeCycleObject).acquire();
														}
				
										XposedBridge.log("cambio di stato AFTER, lifeCycleObject = " + lifeCycleObject +" da S3 a S2, transition: editautomaton.impl.TransitionImpl@533bda92 (interceptedAction: after#android.app.Activity().onResume(), actionToPerform: after#android.app.Activity().onResume();WakeLockEnforcer().resume())");
										currentStates.put(lifeCycleObject,S2);
									}
				
								}
							}
					}			
						
					}
				});
				}catch(Exception e){
					e.printStackTrace();
				}catch(Error e){
				    e.printStackTrace();
				}
				try{
						
					findAndHookMethod(targetClass, "onPause", new XC_MethodHook(){
				
				
					
					public android.os.PowerManager.WakeLock resource = null;
					@SuppressLint("MissingPermission")
					@Override
					protected void beforeHookedMethod(MethodHookParam param) throws Throwable
					{		
									android.app.Activity lifeCycleObjectOBJ = (android.app.Activity)param.thisObject;
									String lifeCycleObject = lifeCycleObjectOBJ.getClass().getCanonicalName();
							
							String idMappa = lifeCycleObject + "after#android.app.Activity().onPause()";
							XposedBridge.log("BEFORE###after#android.app.Activity().onPause()" + lifeCycleObject);
							if (!doNotAlterateExecution){	
								super.beforeHookedMethod(param);
								if ((method2hooked.get("after#android.app.Activity().onPause()")==null)||!(method2hooked.get("after#android.app.Activity().onPause()"))){
					            	method2hooked.put("after#android.app.Activity().onPause()", true);
									lifeCycleObject2class.put(idMappa, param.method.getDeclaringClass().toString());
				
				
								}
							}		}
							
						
				
				
					@SuppressLint("MissingPermission")	
					@Override
					protected void afterHookedMethod(MethodHookParam param) throws Throwable
					{	
									android.app.Activity lifeCycleObjectOBJ = (android.app.Activity)param.thisObject;
									String lifeCycleObject = lifeCycleObjectOBJ.getClass().getCanonicalName();
						
						String idMappa = lifeCycleObject + "after#android.app.Activity().onPause()";
						XposedBridge.log("AFTER###after#android.app.Activity().onPause()" + lifeCycleObject);
						if (!doNotAlterateExecution)
						{	
							super.afterHookedMethod(param);
							if(method2hooked.get("after#android.app.Activity().onPause()")) {
								XposedBridge.log("lifeCycleObject2class: " + lifeCycleObject2class.get(idMappa) + "/// param: " + param.method.getDeclaringClass().toString());
								if(lifeCycleObject2class.get(idMappa)!= null && lifeCycleObject2class.get(idMappa).equals(param.method.getDeclaringClass().toString())){
									method2hooked.put("after#android.app.Activity().onPause()", false);
									lifeCycleObject2class.put(idMappa, "");
				
				
									if(currentStates.get(lifeCycleObject)!= null && currentStates.get(lifeCycleObject) == S1)         
									{
				
										XposedBridge.log("cambio di stato AFTER, lifeCycleObject = " + lifeCycleObject +" da S1 a S0, transition: editautomaton.impl.TransitionImpl@433defed (interceptedAction: after#android.app.Activity().onPause(), actionToPerform: after#android.app.Activity().onPause())");
										currentStates.put(lifeCycleObject,S0);
									}
									else if (currentStates.get(lifeCycleObject)!= null && currentStates.get(lifeCycleObject) == S2)
									{
								doNotAlterateExecution = true;
											XposedBridge.log("DNA = TRUE after#android.os.PowerManager.WakeLock().release()");
								            if(!((resource!=null && resource.equals(param.thisObject)) || instance.equals(param.thisObject))){
								            	XposedBridge.log("non instance");
												if (resource!=null){
								            		resource.release();
								            	}else{
								            		instance.release();
												}
								            }
								            doNotAlterateExecution = false;
				
										XposedBridge.log("cambio di stato AFTER, lifeCycleObject = " + lifeCycleObject +" da S2 a S3, transition: editautomaton.impl.TransitionImpl@7fd50002 (interceptedAction: after#android.app.Activity().onPause(), actionToPerform: after#android.app.Activity().onPause();after#android.os.PowerManager.WakeLock().release())");
										currentStates.put(lifeCycleObject,S3);
									}
				
								}
							}
					}			
						
					}
				});
				}catch(Exception e){
					e.printStackTrace();
				}catch(Error e){
				    e.printStackTrace();
				}
				try{
						
					findAndHookMethod(targetClass, "onCreate", android.os.Bundle.class, new XC_MethodHook(){
				
				
					
					public android.os.PowerManager.WakeLock resource = null;
					@SuppressLint("MissingPermission")
					@Override
					protected void beforeHookedMethod(MethodHookParam param) throws Throwable
					{		
									android.app.Activity lifeCycleObjectOBJ = (android.app.Activity)param.thisObject;
									String lifeCycleObject = lifeCycleObjectOBJ.getClass().getCanonicalName();
							
							String idMappa = lifeCycleObject + "before#android.app.Activity().onCreate(android.os.Bundle)";
							XposedBridge.log("BEFORE###before#android.app.Activity().onCreate(android.os.Bundle)" + lifeCycleObject);
							if (!doNotAlterateExecution){	
								super.beforeHookedMethod(param);
								if ((method2hooked.get("before#android.app.Activity().onCreate(android.os.Bundle)")==null)||!(method2hooked.get("before#android.app.Activity().onCreate(android.os.Bundle)"))){
					            	method2hooked.put("before#android.app.Activity().onCreate(android.os.Bundle)", true);
									lifeCycleObject2class.put(idMappa, param.method.getDeclaringClass().toString());
				
				
								if (currentStates.get(lifeCycleObject)!= null && currentStates.get(lifeCycleObject) == S0)         
								{
								
								
									currentStates.put(lifeCycleObject,S1);
									XposedBridge.log("cambio di stato BEFORE, lifeCycleObject = " + lifeCycleObject + "da S0 a S1, transition: editautomaton.impl.TransitionImpl@548a24a (interceptedAction: before#android.app.Activity().onCreate(android.os.Bundle), actionToPerform: before#android.app.Activity().onCreate(android.os.Bundle))");
									XposedBridge.log("lifecycleObject = " + lifeCycleObject);
									
									
					
								}
								}
							}		}
							
						
				
				
					@SuppressLint("MissingPermission")	
					@Override
					protected void afterHookedMethod(MethodHookParam param) throws Throwable
					{	
									android.app.Activity lifeCycleObjectOBJ = (android.app.Activity)param.thisObject;
									String lifeCycleObject = lifeCycleObjectOBJ.getClass().getCanonicalName();
						
						String idMappa = lifeCycleObject + "before#android.app.Activity().onCreate(android.os.Bundle)";
						XposedBridge.log("AFTER###before#android.app.Activity().onCreate(android.os.Bundle)" + lifeCycleObject);
						if (!doNotAlterateExecution)
						{	
							super.afterHookedMethod(param);
							if(method2hooked.get("before#android.app.Activity().onCreate(android.os.Bundle)")) {
								XposedBridge.log("lifeCycleObject2class: " + lifeCycleObject2class.get(idMappa) + "/// param: " + param.method.getDeclaringClass().toString());
								if(lifeCycleObject2class.get(idMappa)!= null && lifeCycleObject2class.get(idMappa).equals(param.method.getDeclaringClass().toString())){
									method2hooked.put("before#android.app.Activity().onCreate(android.os.Bundle)", false);
									lifeCycleObject2class.put(idMappa, "");
				
				
									if(currentStates.get(lifeCycleObject)!= null && currentStates.get(lifeCycleObject) == S0)         
									{
				
										XposedBridge.log("cambio di stato AFTER, lifeCycleObject = " + lifeCycleObject +" da S0 a S1, transition: editautomaton.impl.TransitionImpl@548a24a (interceptedAction: before#android.app.Activity().onCreate(android.os.Bundle), actionToPerform: before#android.app.Activity().onCreate(android.os.Bundle))");
										currentStates.put(lifeCycleObject,S1);
									}
				
								}
							}
					}			
						
					}
				});
				}catch(Exception e){
					e.printStackTrace();
				}catch(Error e){
				    e.printStackTrace();
				}

		}
		            boolean isTargetHandledObject = false;
            try {
                isTargetHandledObject = handledObjectClass.isAssignableFrom(targetClass);
				if(isTargetHandledObject){
					XposedBridge.log("OK" + "lifeCycleObjectClazz: " + handledObjectClass + "//////" + "targetClass: " + targetClass);
					classList.add(targetClass.toString().replace("class ", ""));
				}
            } catch (Exception e) {
                //e.printStackTrace();
            } catch (Error e){
				
			}
            if (isTargetHandledObject) {


				try{
						
					findAndHookMethod("android.os.PowerManager.WakeLock", lpparam.classLoader, "release", new XC_MethodHook(){
				
				
					
					public android.os.PowerManager.WakeLock resource = null;
					@SuppressLint("MissingPermission")
					@Override
					protected void beforeHookedMethod(MethodHookParam param) throws Throwable
					{		
									String lifeCycleObject = mCurrentLifeCycleObject;
										
							
							String idMappa = lifeCycleObject + "after#android.os.PowerManager.WakeLock().release()";
							XposedBridge.log("BEFORE###after#android.os.PowerManager.WakeLock().release()" + lifeCycleObject);
							if (!doNotAlterateExecution){	
								super.beforeHookedMethod(param);
								if ((method2hooked.get("after#android.os.PowerManager.WakeLock().release()")==null)||!(method2hooked.get("after#android.os.PowerManager.WakeLock().release()"))){
					            	method2hooked.put("after#android.os.PowerManager.WakeLock().release()", true);
									lifeCycleObject2class.put(idMappa, param.method.getDeclaringClass().toString());
									
									XposedBridge.log("salvati parametri");
									if(lifeCycleObject2resource.get(lifeCycleObject)==null && instance.equals(param.thisObject)){
										lifeCycleObject2resource.put(lifeCycleObject, instance);
									}
									resource = lifeCycleObject2resource.get(lifeCycleObject);
									
				
				
								}
							}		}
							
						
				
				
					@SuppressLint("MissingPermission")	
					@Override
					protected void afterHookedMethod(MethodHookParam param) throws Throwable
					{	
									String lifeCycleObject = mCurrentLifeCycleObject;
										
						
						String idMappa = lifeCycleObject + "after#android.os.PowerManager.WakeLock().release()";
						XposedBridge.log("AFTER###after#android.os.PowerManager.WakeLock().release()" + lifeCycleObject);
						if (!doNotAlterateExecution)
						{	
							super.afterHookedMethod(param);
							if(method2hooked.get("after#android.os.PowerManager.WakeLock().release()")) {
								XposedBridge.log("lifeCycleObject2class: " + lifeCycleObject2class.get(idMappa) + "/// param: " + param.method.getDeclaringClass().toString());
								if(lifeCycleObject2class.get(idMappa)!= null && lifeCycleObject2class.get(idMappa).equals(param.method.getDeclaringClass().toString())){
									method2hooked.put("after#android.os.PowerManager.WakeLock().release()", false);
									lifeCycleObject2class.put(idMappa, "");
				
				
									if(currentStates.get(lifeCycleObject)!= null && currentStates.get(lifeCycleObject) == S2)         
									{
				
										XposedBridge.log("cambio di stato AFTER, lifeCycleObject = " + lifeCycleObject +" da S2 a S1, transition: editautomaton.impl.TransitionImpl@3f4faf53 (interceptedAction: after#android.os.PowerManager.WakeLock().release(), actionToPerform: after#android.os.PowerManager.WakeLock().release())");
										currentStates.put(lifeCycleObject,S1);
									}
				
							
										doNotAlterateExecution = true;
										XposedBridge.log("DNA = TRUE after#android.os.PowerManager.WakeLock().release()");
										if(!((resource!=null && resource.equals(param.thisObject)) || instance.equals(param.thisObject))){
										  	if (resource!=null)
										    	resource.release();
										    else
										     	instance.release();
										}
										doNotAlterateExecution = false;
									
				
								}
							}
					}			
						
					}
				});
				}catch(Exception e){
					e.printStackTrace();
				}catch(Error e){
				    e.printStackTrace();
				}
				try{
						
					findAndHookMethod("android.os.PowerManager.WakeLock", lpparam.classLoader, "acquire", new XC_MethodHook(){
				
				
					
					public android.os.PowerManager.WakeLock resource = null;
					@SuppressLint("MissingPermission")
					@Override
					protected void beforeHookedMethod(MethodHookParam param) throws Throwable
					{		
									String lifeCycleObject = mCurrentLifeCycleObject;
										
							
							String idMappa = lifeCycleObject + "after#android.os.PowerManager.WakeLock().acquire()";
							XposedBridge.log("BEFORE###after#android.os.PowerManager.WakeLock().acquire()" + lifeCycleObject);
							if (!doNotAlterateExecution){	
								super.beforeHookedMethod(param);
								if ((method2hooked.get("after#android.os.PowerManager.WakeLock().acquire()")==null)||!(method2hooked.get("after#android.os.PowerManager.WakeLock().acquire()"))){
					            	method2hooked.put("after#android.os.PowerManager.WakeLock().acquire()", true);
									lifeCycleObject2class.put(idMappa, param.method.getDeclaringClass().toString());
									
									XposedBridge.log("salvati parametri");
									if(lifeCycleObject2resource.get(lifeCycleObject)==null && instance.equals(param.thisObject)){
										lifeCycleObject2resource.put(lifeCycleObject, instance);
									}
									resource = lifeCycleObject2resource.get(lifeCycleObject);
									
				
				
								}
							}		}
							
						
				
				
					@SuppressLint("MissingPermission")	
					@Override
					protected void afterHookedMethod(MethodHookParam param) throws Throwable
					{	
									String lifeCycleObject = mCurrentLifeCycleObject;
										
						
						String idMappa = lifeCycleObject + "after#android.os.PowerManager.WakeLock().acquire()";
						XposedBridge.log("AFTER###after#android.os.PowerManager.WakeLock().acquire()" + lifeCycleObject);
						if (!doNotAlterateExecution)
						{	
							super.afterHookedMethod(param);
							if(method2hooked.get("after#android.os.PowerManager.WakeLock().acquire()")) {
								XposedBridge.log("lifeCycleObject2class: " + lifeCycleObject2class.get(idMappa) + "/// param: " + param.method.getDeclaringClass().toString());
								if(lifeCycleObject2class.get(idMappa)!= null && lifeCycleObject2class.get(idMappa).equals(param.method.getDeclaringClass().toString())){
									method2hooked.put("after#android.os.PowerManager.WakeLock().acquire()", false);
									lifeCycleObject2class.put(idMappa, "");
				
				
									if(currentStates.get(lifeCycleObject)!= null && currentStates.get(lifeCycleObject) == S1)         
									{
				
										XposedBridge.log("cambio di stato AFTER, lifeCycleObject = " + lifeCycleObject +" da S1 a S2, transition: editautomaton.impl.TransitionImpl@2a693f59 (interceptedAction: after#android.os.PowerManager.WakeLock().acquire(), actionToPerform: after#android.os.PowerManager.WakeLock().acquire())");
										currentStates.put(lifeCycleObject,S2);
									}
				
							
										doNotAlterateExecution = true;
										XposedBridge.log("DNA = TRUE after#android.os.PowerManager.WakeLock().acquire()");
										if(!((resource!=null && resource.equals(param.thisObject)) || instance.equals(param.thisObject))){
										  	if (resource!=null)
										    	resource.acquire();
										    else
										     	instance.acquire();
										}
										doNotAlterateExecution = false;
									
				
								}
							}
					}			
						
					}
				});
				}catch(Exception e){
					e.printStackTrace();
				}catch(Error e){
				    e.printStackTrace();
				}
				try{
						
					findAndHookMethod("android.os.PowerManager.WakeLock", lpparam.classLoader, "acquire", long.class, new XC_MethodHook(){
				
				
					
					public android.os.PowerManager.WakeLock resource = null;
					@SuppressLint("MissingPermission")
					@Override
					protected void beforeHookedMethod(MethodHookParam param) throws Throwable
					{		
									String lifeCycleObject = mCurrentLifeCycleObject;
										
							
							String idMappa = lifeCycleObject + "after#android.os.PowerManager.WakeLock().acquire(long)";
							XposedBridge.log("BEFORE###after#android.os.PowerManager.WakeLock().acquire(long)" + lifeCycleObject);
							if (!doNotAlterateExecution){	
								super.beforeHookedMethod(param);
								if ((method2hooked.get("after#android.os.PowerManager.WakeLock().acquire(long)")==null)||!(method2hooked.get("after#android.os.PowerManager.WakeLock().acquire(long)"))){
					            	method2hooked.put("after#android.os.PowerManager.WakeLock().acquire(long)", true);
									lifeCycleObject2class.put(idMappa, param.method.getDeclaringClass().toString());
									
									XposedBridge.log("salvati parametri");
									if(lifeCycleObject2resource.get(lifeCycleObject)==null && instance.equals(param.thisObject)){
										lifeCycleObject2resource.put(lifeCycleObject, instance);
									}
									resource = lifeCycleObject2resource.get(lifeCycleObject);
									
				
				
						
						if((resource!=null && resource.equals(param.thisObject)) || instance.equals(param.thisObject)){
				        	return;
				        }
				        param.setResult(null);
								}
							}		}
							
						
				
				
					@SuppressLint("MissingPermission")	
					@Override
					protected void afterHookedMethod(MethodHookParam param) throws Throwable
					{	
									String lifeCycleObject = mCurrentLifeCycleObject;
										
						
						String idMappa = lifeCycleObject + "after#android.os.PowerManager.WakeLock().acquire(long)";
						XposedBridge.log("AFTER###after#android.os.PowerManager.WakeLock().acquire(long)" + lifeCycleObject);
						if (!doNotAlterateExecution)
						{	
							super.afterHookedMethod(param);
							if(method2hooked.get("after#android.os.PowerManager.WakeLock().acquire(long)")) {
								XposedBridge.log("lifeCycleObject2class: " + lifeCycleObject2class.get(idMappa) + "/// param: " + param.method.getDeclaringClass().toString());
								if(lifeCycleObject2class.get(idMappa)!= null && lifeCycleObject2class.get(idMappa).equals(param.method.getDeclaringClass().toString())){
									method2hooked.put("after#android.os.PowerManager.WakeLock().acquire(long)", false);
									lifeCycleObject2class.put(idMappa, "");
				
				
									if(currentStates.get(lifeCycleObject)!= null && currentStates.get(lifeCycleObject) == S1)         
									{
				
										XposedBridge.log("cambio di stato AFTER, lifeCycleObject = " + lifeCycleObject +" da S1 a S2, transition: editautomaton.impl.TransitionImpl@304bb45b (interceptedAction: after#android.os.PowerManager.WakeLock().acquire(long), actionToPerform: after#android.os.PowerManager.WakeLock().acquire(long))");
										currentStates.put(lifeCycleObject,S2);
									}
				
							
										doNotAlterateExecution = true;
										XposedBridge.log("DNA = TRUE after#android.os.PowerManager.WakeLock().acquire(long)");
										if(!((resource!=null && resource.equals(param.thisObject)) || instance.equals(param.thisObject))){
										  	if (resource!=null)
										    	resource.acquire((long)param.args[0]);
										    else
										     	instance.acquire((long)param.args[0]);
										}
										doNotAlterateExecution = false;
									
				
								}
							}
					}			
						
					}
				});
				}catch(Exception e){
					e.printStackTrace();
				}catch(Error e){
				    e.printStackTrace();
				}

		}
	}

		
		Class<?> targetClass = XposedHelpers.findClass("android.os.PowerManager.WakeLock", lpparam.classLoader);
		
		

	
							
				findAndHookMethod(XposedHelpers.findClass("android.os.PowerManager$WakeLock", lpparam.classLoader), "isHeld", new XC_MethodHook() {
				        			public android.os.PowerManager.WakeLock resource = null;
									@Override
									@SuppressLint("MissingPermission")
							        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
										String lifeCycleObject = mCurrentLifeCycleObject;
																				XposedBridge.log("invocato BEFORE isHeld([])");
										if (doNotAlterateExecution) return;
							            
										resource = lifeCycleObject2resource.get(lifeCycleObject);
							            super.beforeHookedMethod(param);
							            if((resource!=null && resource.equals(param.thisObject)) || instance.equals(param.thisObject)){
							                return;
							            }
							            param.setResult(null);
							        }
				
							        @Override
									@SuppressLint("MissingPermission")
							        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
							            String stackTrace = getStackTrace();
										boolean found = false;
										for(int g = 0; g<classList.size(); g++){
											if(stackTrace.contains(classList.get(g)))
												found = true;
										}
										if(!found){
											return;
										}
										XposedBridge.log("invocato AFTER isHeld([])");
										if (doNotAlterateExecution) return;
							            super.afterHookedMethod(param);
										doNotAlterateExecution = true;
										
							            if(!((resource!=null && resource.equals(param.thisObject)) || instance.equals(param.thisObject))){
							            	if (resource!=null)
							                	param.setResult(resource.isHeld());
							                else
							                	param.setResult(instance.isHeld());
										}
										doNotAlterateExecution = false;
							        }
						    	});
							
							
				findAndHookMethod(XposedHelpers.findClass("android.os.PowerManager$WakeLock", lpparam.classLoader), "setWorkSource", android.os.WorkSource.class, new XC_MethodHook() {
				        			public android.os.PowerManager.WakeLock resource = null;
									@Override
									@SuppressLint("MissingPermission")
							        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
										String lifeCycleObject = mCurrentLifeCycleObject;
											
										XposedBridge.log("invocato BEFORE setWorkSource([android.os.WorkSource])");
										if (doNotAlterateExecution) return;
										resource = lifeCycleObject2resource.get(lifeCycleObject);
							            super.beforeHookedMethod(param);
							            if((resource!=null && resource.equals(param.thisObject)) || instance.equals(param.thisObject)){
							                return;
							            }
							            param.setResult(null);
							        }
				
							        @Override
									@SuppressLint("MissingPermission")
							        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
							            String stackTrace = getStackTrace();
										boolean found = false;
										for(int g = 0; g<classList.size(); g++){
											if(stackTrace.contains(classList.get(g)))
												found = true;
										}
										if(!found){
											return;
										}
										XposedBridge.log("invocato AFTER setWorkSource([android.os.WorkSource])");
										if (doNotAlterateExecution) return;
							            super.afterHookedMethod(param);
										doNotAlterateExecution = true;
										
							            if(!((resource!=null && resource.equals(param.thisObject)) || instance.equals(param.thisObject))){
							            	if (resource!=null)
							                	resource.setWorkSource((WorkSource)param.args[0]);
							                else
							                	instance.setWorkSource((WorkSource)param.args[0]);
										}
										doNotAlterateExecution = false;
							        }
						    	});
							
							
				findAndHookMethod(XposedHelpers.findClass("android.os.PowerManager$WakeLock", lpparam.classLoader), "setReferenceCounted", boolean.class, new XC_MethodHook() {
				        			public android.os.PowerManager.WakeLock resource = null;
									@Override
									@SuppressLint("MissingPermission")
							        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
										String lifeCycleObject = mCurrentLifeCycleObject;
											
										XposedBridge.log("invocato BEFORE setReferenceCounted([boolean])");
										if (doNotAlterateExecution) return;
										resource = lifeCycleObject2resource.get(lifeCycleObject);
							            super.beforeHookedMethod(param);
							            if((resource!=null && resource.equals(param.thisObject)) || instance.equals(param.thisObject)){
							                return;
							            }
							            param.setResult(null);
							        }
				
							        @Override
									@SuppressLint("MissingPermission")
							        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
							            String stackTrace = getStackTrace();
										boolean found = false;
										for(int g = 0; g<classList.size(); g++){
											if(stackTrace.contains(classList.get(g)))
												found = true;
										}
										if(!found){
											return;
										}
										XposedBridge.log("invocato AFTER setReferenceCounted([boolean])");
										if (doNotAlterateExecution) return;
							            super.afterHookedMethod(param);
										doNotAlterateExecution = true;
										
							            if(!((resource!=null && resource.equals(param.thisObject)) || instance.equals(param.thisObject))){
							            	if (resource!=null)
							                	resource.setReferenceCounted((boolean)param.args[0]);
							                else
							                	instance.setReferenceCounted((boolean)param.args[0]);
										}
										doNotAlterateExecution = false;
							        }
						    	});
							
							
				findAndHookMethod(XposedHelpers.findClass("android.os.PowerManager$WakeLock", lpparam.classLoader), "toString", new XC_MethodHook() {
				        			public android.os.PowerManager.WakeLock resource = null;
									@Override
									@SuppressLint("MissingPermission")
							        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
										String lifeCycleObject = mCurrentLifeCycleObject;
																				XposedBridge.log("invocato BEFORE toString([])");
										if (doNotAlterateExecution) return;
							            
										resource = lifeCycleObject2resource.get(lifeCycleObject);
							            super.beforeHookedMethod(param);
							            if((resource!=null && resource.equals(param.thisObject)) || instance.equals(param.thisObject)){
							                return;
							            }
							            param.setResult(null);
							        }
				
							        @Override
									@SuppressLint("MissingPermission")
							        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
							            String stackTrace = getStackTrace();
										boolean found = false;
										for(int g = 0; g<classList.size(); g++){
											if(stackTrace.contains(classList.get(g)))
												found = true;
										}
										if(!found){
											return;
										}
										XposedBridge.log("invocato AFTER toString([])");
										if (doNotAlterateExecution) return;
							            super.afterHookedMethod(param);
										doNotAlterateExecution = true;
										
							            if(!((resource!=null && resource.equals(param.thisObject)) || instance.equals(param.thisObject))){
							            	if (resource!=null)
							                	param.setResult(resource.toString());
							                else
							                	param.setResult(instance.toString());
										}
										doNotAlterateExecution = false;
							        }
						    	});
							
							
				findAndHookMethod(XposedHelpers.findClass("android.os.PowerManager$WakeLock", lpparam.classLoader), "release", int.class, new XC_MethodHook() {
				        			public android.os.PowerManager.WakeLock resource = null;
									@Override
									@SuppressLint("MissingPermission")
							        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
										String lifeCycleObject = mCurrentLifeCycleObject;
											
										XposedBridge.log("invocato BEFORE release([int])");
										if (doNotAlterateExecution) return;
										resource = lifeCycleObject2resource.get(lifeCycleObject);
							            super.beforeHookedMethod(param);
							            if((resource!=null && resource.equals(param.thisObject)) || instance.equals(param.thisObject)){
							                return;
							            }
							            param.setResult(null);
							        }
				
							        @Override
									@SuppressLint("MissingPermission")
							        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
							            String stackTrace = getStackTrace();
										boolean found = false;
										for(int g = 0; g<classList.size(); g++){
											if(stackTrace.contains(classList.get(g)))
												found = true;
										}
										if(!found){
											return;
										}
										XposedBridge.log("invocato AFTER release([int])");
										if (doNotAlterateExecution) return;
							            super.afterHookedMethod(param);
										doNotAlterateExecution = true;
										
							            if(!((resource!=null && resource.equals(param.thisObject)) || instance.equals(param.thisObject))){
							            	if (resource!=null)
							                	resource.release((int)param.args[0]);
							                else
							                	instance.release((int)param.args[0]);
										}
										doNotAlterateExecution = false;
							        }
						    	});
							
	
	

						hookAllConstructors(findClass("android.app.Activity", lpparam.classLoader), new XC_MethodHook() {
				
							        @Override
							        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
							            
										if (doNotAlterateExecution) return;
										super.beforeHookedMethod(param);
							            XposedBridge.log("invocato COSTRUTTORE BEFORE" + "android.app.Activity");
	                    				doNotAlterateExecution=true;
							        }
	
							        @Override
							        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
							            android.app.Activity lifeCycleObjectOBJ = (android.app.Activity)param.thisObject;
										String lifeCycleObject = lifeCycleObjectOBJ.getClass().getCanonicalName();
										super.afterHookedMethod(param);									
							
										XposedBridge.log("invocato COSTRUTTORE AFTER " + "android.app.Activity");
							            doNotAlterateExecution=false;
							        }
						    	});
						hookAllConstructors(findClass("android.os.PowerManager.WakeLock", lpparam.classLoader), new XC_MethodHook() {
				
							        @Override
							        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
							            String lifeCycleObject = mCurrentLifeCycleObject;
							            	
										if (doNotAlterateExecution) return;
										super.beforeHookedMethod(param);
							            XposedBridge.log("invocato COSTRUTTORE BEFORE" + "android.os.PowerManager.WakeLock");
	                    				doNotAlterateExecution=true;
							        }
	
							        @Override
							        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
							            String lifeCycleObject = mCurrentLifeCycleObject;
							            		
										super.afterHookedMethod(param);
										String stackTrace = getStackTrace();
										for(String el : classList){
											if(stackTrace.contains(el)){
												instance = (android.os.PowerManager.WakeLock) param.thisObject;
												lifeCycleObject2resource.put(lifeCycleObject,instance);
											}
										}
									
										XposedBridge.log("invocato COSTRUTTORE AFTER " + "android.os.PowerManager.WakeLock");
							            doNotAlterateExecution=false;
							           
							        }
						    	});
	
	

	}
		
	public String getStackTrace() {
	        StringWriter sw = new StringWriter();
	        PrintWriter pw = new PrintWriter(sw);
	        new Throwable().printStackTrace(pw);
	        String sStackTrace = sw.toString();
	        return sStackTrace;
	    }

}
