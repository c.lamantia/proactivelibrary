package org.eclipse.acceleo.module.sample.services;

import java.lang.reflect.Constructor;
import java.net.InterfaceAddress;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.google.common.primitives.Primitives;

import android.os.IInterface;

import java.lang.reflect.*;

public class Service {
	public static String FILENAME = "/home/simone/runtime-EclipseApplication/WebPcSuiteWakeLock/default.editautomaton";
	
	public ArrayList<String> getPublicAndInheritedMethods(String api)throws ClassNotFoundException{
		ArrayList<String> list=new ArrayList<>();
		if(api.contentEquals("empty")){
			list.add(api);
			return list;
		}else{
			ArrayList<String> lista2=new ArrayList<>();
			//System.out.println("classe : " + api);
			Class<?> clazz;
			String checkInner = innerClassCheck(api);
			//System.out.println("api: " + api + " check inner "+ checkInner);
			if(checkInner.equals("")){
				System.out.println(api);
				clazz = Class.forName(api);
			}else{
				clazz = Class.forName(checkInner);
			}
			Method[] metodi = clazz.getDeclaredMethods();
			//Method[] metodi = clazz.getMethods();
			for(Method met : metodi){
				if (Modifier.isPublic(met.getModifiers())){
					Class<?> declaredClass = met.getDeclaringClass();
					Class[] parametro = met.getParameterTypes();
					for(Class parametro1 : parametro){
						lista2.add(parametro1.getName());	
					}
					list.add(met.toGenericString() + "%" + met.getReturnType().toString() + "%" + declaredClass + "%" + met.getName() +"(" + lista2 + ")");
					lista2.clear();
				}		
			}
			return list;
		}
	}
	
	public String getVarList(String methodString,  String api)throws Exception{
		ArrayList<String> list=new ArrayList<>();
		String methodName = "";
		String args = ""; 
		for(int i = 0; i<methodString.length(); i++){
			if(methodString.charAt(i)=='('){
				methodName = methodString.substring(0, i);
				args = methodString.substring(i+1, methodString.length()-1);
			}
		}
		//System.out.println("api: " + api);
		//System.out.println("metName: " + methodName);
		//System.out.println("args: " + args); 
		
		args = args.replaceAll(" ","");
		String[] parts = args.split(",");
		Class<?>[] classList = new Class[parts.length];
		boolean flagArgs = false;
		if(!args.equals("")){
			flagArgs = true;
			//System.out.println("ci sono argomenti");
			for(int i = 0; i<parts.length; i++){
				//System.out.println("argomento : " + parts[i]);
				parts[i] = parts[i].replace(" ", "");
				//System.out.println(parts[i]);
				if(parts[i].equals("int")||parts[i].equals("long")||parts[i].equals("byte")||parts[i].equals("float")||parts[i].equals("double")||parts[i].equals("boolean")||parts[i].equals("char")||parts[i].equals("short")){
					classList[i] = parseType(parts[i]);
				}else{
					String strNoPar = parts[i].replaceAll("\\[", "").replaceAll("\\]","");
					if(strNoPar.equals(parts[i])){
						Class<?> cl = Class.forName(parts[i]);
						classList[i] = cl;
					}else{
						String o = "[L" + strNoPar + ";";
						//System.out.println("stringa mod" + o);
						Class<?> cl = Class.forName(o);
						classList[i] = cl;
					}
					
				}		
			}	
		}
		
		
		Class<?> clazz;		
		String checkInner = innerClassCheck(api);
		//System.out.println("api: " + api + " check inner "+ checkInner);
		if(checkInner.equals("")){
			clazz = Class.forName(api);
		}else{
			clazz = Class.forName(checkInner);
		}
		
		Method method;
		if(!flagArgs){
			method = clazz.getMethod(methodName);
		}else{
			method = clazz.getMethod(methodName, classList);
		}
		
		
		Class[] parametro = method.getParameterTypes();
		for(Class parametro1 : parametro){
			list.add(parametro1.getName());
		}
		String output = method.getReturnType().toString().replaceAll("class ", "") + "%" + method.getName() + "(" + list + ")";
		return output;
	}
	
	public String innerClassCheck(String api){
		String out = "";
		String[] parts = api.split("\\.");
		boolean classFound = false;
		boolean flag = false;
		for(String part: parts){
			if(Character.isUpperCase(part.charAt(0))){
				if(classFound){
					flag = true;
				}else{
					classFound = true;
				}
			}
		}
		int len = parts.length;
		if(flag){
			for(int i = 0; i<len ; i++){
				if (i!= len-2){
					out = out + parts[i] + "."; 
				}else{
					out = out +parts[i] + "$";
				}
			}
		}
		if(out.length()>1){
			out = out.substring(0,out.length()-1);
		}
		return out;
	}
	
	public String fileContained(String s1) throws FileNotFoundException{
		Scanner scanner = new Scanner(new File(FILENAME));
		List<String> list=new ArrayList<>();
		while(scanner.hasNextLine()){
			list.add(scanner.nextLine()); 
		 }
		for(String str: list) {
			if(str.trim().contains(s1))
				return "contained";
 		 }
		 		return "notcontained";
	}
	
	public static boolean isAutomataAction(String method){ 
        BufferedReader br = null;
        FileReader fr = null;
        try {
            fr = new FileReader(FILENAME);
            br = new BufferedReader(fr);
            String line;
            method = method.replace("[", "").replace("]", "");
            String strPattern = method.replace('$', '.');
            while ((line = br.readLine()) != null) {
                if(line.contains(strPattern)){
                    return true;
                }else if (line.contains(strPattern.replace("(", "").replace(")", ""))){
                    return true;
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return false;
    }
	
	public String getApi() throws Throwable{
		return getHandledObject();
	}
	
	public static String extractParameters(String method){
		//System.out.println("METODO" + method);
		int startIndex = 0;
		boolean flag = false;
	    for(int index = 0; index<method.length(); index++){
	    	if(flag == false && method.charAt(index) == '['){
	    		flag = true;
	    		startIndex = index + 1;
	        }else if(method.charAt(index)== ']'){
	        	String paramStr = method.substring(startIndex, index);
                String[] parameters = paramStr.split(",");
                if(paramStr.equals("")){
                    return paramStr;
                }else{
                    paramStr = "";
                    for(String parameter : parameters){
                    	//System.out.println("paramtero" + parameter);
                        String parameterMod = parameter.replace('$', '.').replaceAll(" ", "");
                        if(parameterMod.equals("[B")){
                            parameterMod = "byte[]";
                        }else if (parameterMod.equals("[Z")){
                            parameterMod = "boolean[]";
                        }else if(parameterMod.equals("[C")){
                            parameterMod = "char[]";
                        }else if(parameterMod.equals("[S")){
                            parameterMod = "short[]";
                        }else if(parameterMod.equals("[I")){
                            parameterMod = "int[]";
                        }else if(parameterMod.equals("[J")){
                            parameterMod = "long[]";
                        }else if(parameterMod.equals("[F")){
                            parameterMod = "float[]";
                        }else if(parameterMod.equals("[D")){
                            parameterMod = "double[]";
                        }else if(parameterMod.contains("[Ljava")){
                        	String[] parts = parameterMod.split("\\.");
                        	int len = parts.length - 1;
                        	parameterMod = parts[len].replace(";","") + "[]";
                        }
                        paramStr = paramStr + parameterMod + ".class, ";
                    }
                    return paramStr;
                }
                
	        }
        }        
        return null;
    }
	
	public String extractClass(String api){
		String flag = innerClassCheck(api);
		String[] parts = api.split("\\.");
		boolean found = false;
		if(flag.equals("")){
			for(String part: parts){
				if(Character.isUpperCase(part.charAt(0))){
					part = part.replace("()", "");
					return part;
				}
			}
		}else{
			String out = "";
			for(String part: parts){
				if(Character.isUpperCase(part.charAt(0))){
					if(!found){
						found = true;
						out = out + part + ".";
					}else{
						out = out + part;
					}
					part = part.replace("()", "");
				}
			}
			return out; 
		}
		
		return null;
	}
	
	public static String extractMethodName(String method){
        for(int index = 0; index<method.length(); index++){
            if(method.charAt(index) == '('){
                return(method.substring(0,index));   
            }
        }
        return null;
    }
	
	public static String setParameters(String paramString){
        String output = "";
        if(paramString.equals("")){
            return "";
        }else{
            paramString = paramString.substring(0,paramString.length()-1);
            String[] parameters = paramString.split(",");
            for(int i = 0; i<parameters.length; i++){
                String[] parts = parameters[i].split("\\.");
                String clazz = "";
                boolean foundFirst = false; 
		        for(String part: parts){
                    part = part.replace(" ","");
			        if(Character.isUpperCase(part.charAt(0))){
                        if(!foundFirst){
                            clazz = part;
                           foundFirst = true;                             
                        }else{
                            clazz = clazz + "." + part;
                        }                    
                    }else if(part.contains("int") | part.contains("boolean") | part.contains("char") | part.contains("byte") | part.contains("short") | part.contains("double") | part.contains("long") | part.contains("float")){
                        clazz = part;
                    }
                }
                output = output + "(" + clazz + ")" + "param.args[" + i + "]" + ", ";
		    }        
        }
        return output.substring(0,output.length()-2);
    }
	public static Class<?> parseType(final String className) {
	    switch (className) {
	        case "boolean":
	            return boolean.class;
	        case "byte":
	            return byte.class;
	        case "short":
	            return short.class;
	        case "int":
	            return int.class;
	        case "long":
	            return long.class;
	        case "float":
	            return float.class;
	        case "double":
	            return double.class;
	        case "char":
	            return char.class;
	        case "void":
	            return void.class;
	        default:
	            String fqn = className.contains(".") ? className : "java.lang.".concat(className);
	            try {
	                return Class.forName(fqn);
	            } catch (ClassNotFoundException ex) {
	                throw new IllegalArgumentException("Class not found: " + fqn);
	            }
	    }
	}
	
	public static String getLifeCycleObject() throws Exception{ 
		String text = readFile(FILENAME);		
		Pattern p = Pattern.compile(".*api=\"(.+)\".*");
	    Matcher m = p.matcher(text);
	    while(m.find()){
	        String b =  m.group(1);
	        //System.out.println(b);
	        return b;
	    }
	    return null;
	}
	
	public static String getHandledObject() throws Exception{ 
		String text = readFile(FILENAME);		
		Pattern p = Pattern.compile(".*handledObject=\"([^\"]+)\"");
	    Matcher m = p.matcher(text);
	    while(m.find()){
	        String b =  m.group(1);
	        //System.out.println(b);
	        return b;
	    }
	    return null;
	}

	private static String readFile(String pathname) throws IOException {

	    File file = new File(pathname);
	    StringBuilder fileContents = new StringBuilder((int)file.length());        

	    try (Scanner scanner = new Scanner(file)) {
	        while(scanner.hasNextLine()) {
	            fileContents.append(scanner.nextLine() + System.lineSeparator());
	        }
	        return fileContents.toString();
	    }
	}
	
	public static String doubleMethodCheck(String str){
		System.out.println("entrato!");
		System.out.println("prima " + str);
		str = str.replaceAll("\\(([^\\)]*)\\)", "()").replaceAll("after#", "").replaceAll("before#", "");
		System.out.println("dopo " + str);
		String[] parts = str.split("\\.");
		boolean found = false;
		String out = "";
		for(int i = 0; i<parts.length; i++){
			if(!found){
				if(Character.isUpperCase(parts[i].charAt(0))){
					found = true;
				}
			}else{
				out = out + "." + parts[i];
			}
			
		}
		System.out.println("out = " + out);
		return out.substring(0,out.length()-2);
	}
	
	
	
	
	
	
	
	
	
	
}