package editautomaton.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.RulerGridPreferencePage;

import editautomaton.diagram.part.EditautomatonDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramRulersAndGridPreferencePage extends RulerGridPreferencePage {

	/**
	* @generated
	*/
	public DiagramRulersAndGridPreferencePage() {
		setPreferenceStore(EditautomatonDiagramEditorPlugin.getInstance().getPreferenceStore());
	}
}
