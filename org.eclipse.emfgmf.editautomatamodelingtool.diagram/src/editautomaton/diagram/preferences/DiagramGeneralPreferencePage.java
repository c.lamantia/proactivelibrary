package editautomaton.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.DiagramsPreferencePage;

import editautomaton.diagram.part.EditautomatonDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramGeneralPreferencePage extends DiagramsPreferencePage {

	/**
	* @generated
	*/
	public DiagramGeneralPreferencePage() {
		setPreferenceStore(EditautomatonDiagramEditorPlugin.getInstance().getPreferenceStore());
	}
}
