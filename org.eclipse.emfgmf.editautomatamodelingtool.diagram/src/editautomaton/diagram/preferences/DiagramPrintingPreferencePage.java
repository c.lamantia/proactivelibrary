package editautomaton.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.PrintingPreferencePage;

import editautomaton.diagram.part.EditautomatonDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramPrintingPreferencePage extends PrintingPreferencePage {

	/**
	* @generated
	*/
	public DiagramPrintingPreferencePage() {
		setPreferenceStore(EditautomatonDiagramEditorPlugin.getInstance().getPreferenceStore());
	}
}
