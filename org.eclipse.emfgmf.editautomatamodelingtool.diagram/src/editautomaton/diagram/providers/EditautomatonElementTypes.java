package editautomaton.diagram.providers;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypeImages;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypes;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import editautomaton.EditautomatonPackage;
import editautomaton.diagram.edit.parts.EditAutomatonEditPart;
import editautomaton.diagram.edit.parts.InitialStateEditPart;
import editautomaton.diagram.edit.parts.RegularStateEditPart;
import editautomaton.diagram.edit.parts.TransitionEditPart;
import editautomaton.diagram.part.EditautomatonDiagramEditorPlugin;

/**
 * @generated
 */
public class EditautomatonElementTypes {

	/**
	* @generated
	*/
	private EditautomatonElementTypes() {
	}

	/**
	* @generated
	*/
	private static Map<IElementType, ENamedElement> elements;

	/**
	* @generated
	*/
	private static DiagramElementTypeImages elementTypeImages = new DiagramElementTypeImages(
			EditautomatonDiagramEditorPlugin.getInstance().getItemProvidersAdapterFactory());

	/**
	* @generated
	*/
	private static Set<IElementType> KNOWN_ELEMENT_TYPES;

	/**
	* @generated
	*/
	public static final IElementType EditAutomaton_1000 = getElementType(
			"org.eclipse.emfgmf.editautomatamodelingtool.diagram.EditAutomaton_1000"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType InitialState_2001 = getElementType(
			"org.eclipse.emfgmf.editautomatamodelingtool.diagram.InitialState_2001"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType RegularState_2002 = getElementType(
			"org.eclipse.emfgmf.editautomatamodelingtool.diagram.RegularState_2002"); //$NON-NLS-1$
	/**
	* @generated
	*/
	public static final IElementType Transition_4001 = getElementType(
			"org.eclipse.emfgmf.editautomatamodelingtool.diagram.Transition_4001"); //$NON-NLS-1$

	/**
	* @generated
	*/
	public static ImageDescriptor getImageDescriptor(ENamedElement element) {
		return elementTypeImages.getImageDescriptor(element);
	}

	/**
	* @generated
	*/
	public static Image getImage(ENamedElement element) {
		return elementTypeImages.getImage(element);
	}

	/**
	* @generated
	*/
	public static ImageDescriptor getImageDescriptor(IAdaptable hint) {
		return getImageDescriptor(getElement(hint));
	}

	/**
	* @generated
	*/
	public static Image getImage(IAdaptable hint) {
		return getImage(getElement(hint));
	}

	/**
	* Returns 'type' of the ecore object associated with the hint.
	* 
	* @generated
	*/
	public static ENamedElement getElement(IAdaptable hint) {
		Object type = hint.getAdapter(IElementType.class);
		if (elements == null) {
			elements = new IdentityHashMap<IElementType, ENamedElement>();

			elements.put(EditAutomaton_1000, EditautomatonPackage.eINSTANCE.getEditAutomaton());

			elements.put(InitialState_2001, EditautomatonPackage.eINSTANCE.getInitialState());

			elements.put(RegularState_2002, EditautomatonPackage.eINSTANCE.getRegularState());

			elements.put(Transition_4001, EditautomatonPackage.eINSTANCE.getTransition());
		}
		return (ENamedElement) elements.get(type);
	}

	/**
	* @generated
	*/
	private static IElementType getElementType(String id) {
		return ElementTypeRegistry.getInstance().getType(id);
	}

	/**
	* @generated
	*/
	public static boolean isKnownElementType(IElementType elementType) {
		if (KNOWN_ELEMENT_TYPES == null) {
			KNOWN_ELEMENT_TYPES = new HashSet<IElementType>();
			KNOWN_ELEMENT_TYPES.add(EditAutomaton_1000);
			KNOWN_ELEMENT_TYPES.add(InitialState_2001);
			KNOWN_ELEMENT_TYPES.add(RegularState_2002);
			KNOWN_ELEMENT_TYPES.add(Transition_4001);
		}
		return KNOWN_ELEMENT_TYPES.contains(elementType);
	}

	/**
	* @generated
	*/
	public static IElementType getElementType(int visualID) {
		switch (visualID) {
		case EditAutomatonEditPart.VISUAL_ID:
			return EditAutomaton_1000;
		case InitialStateEditPart.VISUAL_ID:
			return InitialState_2001;
		case RegularStateEditPart.VISUAL_ID:
			return RegularState_2002;
		case TransitionEditPart.VISUAL_ID:
			return Transition_4001;
		}
		return null;
	}

	/**
	* @generated
	*/
	public static final DiagramElementTypes TYPED_INSTANCE = new DiagramElementTypes(elementTypeImages) {

		/**
		* @generated
		*/
		@Override

		public boolean isKnownElementType(IElementType elementType) {
			return editautomaton.diagram.providers.EditautomatonElementTypes.isKnownElementType(elementType);
		}

		/**
		* @generated
		*/
		@Override

		public IElementType getElementTypeForVisualId(int visualID) {
			return editautomaton.diagram.providers.EditautomatonElementTypes.getElementType(visualID);
		}

		/**
		* @generated
		*/
		@Override

		public ENamedElement getDefiningNamedElement(IAdaptable elementTypeAdapter) {
			return editautomaton.diagram.providers.EditautomatonElementTypes.getElement(elementTypeAdapter);
		}
	};

}
