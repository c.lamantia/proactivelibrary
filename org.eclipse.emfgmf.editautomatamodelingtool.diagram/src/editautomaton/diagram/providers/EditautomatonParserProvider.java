package editautomaton.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserService;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ParserHintAdapter;
import org.eclipse.gmf.runtime.notation.View;

import editautomaton.EditautomatonPackage;
import editautomaton.diagram.edit.parts.InitialStateNameEditPart;
import editautomaton.diagram.edit.parts.RegularStateNameEditPart;
import editautomaton.diagram.edit.parts.TransitionActionToPerformEditPart;
import editautomaton.diagram.edit.parts.TransitionInterceptedActionEditPart;
import editautomaton.diagram.parsers.MessageFormatParser;
import editautomaton.diagram.part.EditautomatonVisualIDRegistry;

/**
 * @generated
 */
public class EditautomatonParserProvider extends AbstractProvider implements IParserProvider {

	/**
	* @generated
	*/
	private IParser initialStateName_5001Parser;

	/**
	* @generated
	*/
	private IParser getInitialStateName_5001Parser() {
		if (initialStateName_5001Parser == null) {
			EAttribute[] features = new EAttribute[] { EditautomatonPackage.eINSTANCE.getState_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			initialStateName_5001Parser = parser;
		}
		return initialStateName_5001Parser;
	}

	/**
	* @generated
	*/
	private IParser regularStateName_5002Parser;

	/**
	* @generated
	*/
	private IParser getRegularStateName_5002Parser() {
		if (regularStateName_5002Parser == null) {
			EAttribute[] features = new EAttribute[] { EditautomatonPackage.eINSTANCE.getState_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			regularStateName_5002Parser = parser;
		}
		return regularStateName_5002Parser;
	}

	/**
	* @generated
	*/
	private IParser transitionInterceptedAction_6001Parser;

	/**
	* @generated
	*/
	private IParser getTransitionInterceptedAction_6001Parser() {
		if (transitionInterceptedAction_6001Parser == null) {
			EAttribute[] features = new EAttribute[] {
					EditautomatonPackage.eINSTANCE.getTransition_InterceptedAction() };
			MessageFormatParser parser = new MessageFormatParser(features);
			transitionInterceptedAction_6001Parser = parser;
		}
		return transitionInterceptedAction_6001Parser;
	}

	/**
	* @generated
	*/
	private IParser transitionActionToPerform_6002Parser;

	/**
	* @generated
	*/
	private IParser getTransitionActionToPerform_6002Parser() {
		if (transitionActionToPerform_6002Parser == null) {
			EAttribute[] features = new EAttribute[] { EditautomatonPackage.eINSTANCE.getTransition_ActionToPerform() };
			MessageFormatParser parser = new MessageFormatParser(features);
			transitionActionToPerform_6002Parser = parser;
		}
		return transitionActionToPerform_6002Parser;
	}

	/**
	* @generated
	*/
	protected IParser getParser(int visualID) {
		switch (visualID) {
		case InitialStateNameEditPart.VISUAL_ID:
			return getInitialStateName_5001Parser();
		case RegularStateNameEditPart.VISUAL_ID:
			return getRegularStateName_5002Parser();
		case TransitionInterceptedActionEditPart.VISUAL_ID:
			return getTransitionInterceptedAction_6001Parser();
		case TransitionActionToPerformEditPart.VISUAL_ID:
			return getTransitionActionToPerform_6002Parser();
		}
		return null;
	}

	/**
	* Utility method that consults ParserService
	* @generated
	*/
	public static IParser getParser(IElementType type, EObject object, String parserHint) {
		return ParserService.getInstance().getParser(new HintAdapter(type, object, parserHint));
	}

	/**
	* @generated
	*/
	public IParser getParser(IAdaptable hint) {
		String vid = (String) hint.getAdapter(String.class);
		if (vid != null) {
			return getParser(EditautomatonVisualIDRegistry.getVisualID(vid));
		}
		View view = (View) hint.getAdapter(View.class);
		if (view != null) {
			return getParser(EditautomatonVisualIDRegistry.getVisualID(view));
		}
		return null;
	}

	/**
	* @generated
	*/
	public boolean provides(IOperation operation) {
		if (operation instanceof GetParserOperation) {
			IAdaptable hint = ((GetParserOperation) operation).getHint();
			if (EditautomatonElementTypes.getElement(hint) == null) {
				return false;
			}
			return getParser(hint) != null;
		}
		return false;
	}

	/**
	* @generated
	*/
	private static class HintAdapter extends ParserHintAdapter {

		/**
		* @generated
		*/
		private final IElementType elementType;

		/**
		* @generated
		*/
		public HintAdapter(IElementType type, EObject object, String parserHint) {
			super(object, parserHint);
			assert type != null;
			elementType = type;
		}

		/**
		* @generated
		*/
		public Object getAdapter(Class adapter) {
			if (IElementType.class.equals(adapter)) {
				return elementType;
			}
			return super.getAdapter(adapter);
		}
	}

}
