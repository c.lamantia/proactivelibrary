package editautomaton.diagram.providers;

import editautomaton.diagram.part.EditautomatonDiagramEditorPlugin;

/**
 * @generated
 */
public class ElementInitializers {

	protected ElementInitializers() {
		// use #getInstance to access cached instance
	}

	/**
	* @generated
	*/
	public static ElementInitializers getInstance() {
		ElementInitializers cached = EditautomatonDiagramEditorPlugin.getInstance().getElementInitializers();
		if (cached == null) {
			EditautomatonDiagramEditorPlugin.getInstance().setElementInitializers(cached = new ElementInitializers());
		}
		return cached;
	}
}
