package editautomaton.diagram.part;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.tooling.runtime.update.UpdaterNodeDescriptor;

/**
 * @generated
 */
public class EditautomatonNodeDescriptor extends UpdaterNodeDescriptor {
	/**
	* @generated
	*/
	public EditautomatonNodeDescriptor(EObject modelElement, int visualID) {
		super(modelElement, visualID);
	}

}
