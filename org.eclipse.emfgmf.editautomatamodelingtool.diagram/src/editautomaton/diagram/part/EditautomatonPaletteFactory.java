
package editautomaton.diagram.part;

import java.util.Collections;

import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteGroup;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.tooling.runtime.part.DefaultLinkToolEntry;
import org.eclipse.gmf.tooling.runtime.part.DefaultNodeToolEntry;

import editautomaton.diagram.providers.EditautomatonElementTypes;

/**
 * @generated
 */
public class EditautomatonPaletteFactory {

	/**
	* @generated
	*/
	public void fillPalette(PaletteRoot paletteRoot) {
		paletteRoot.add(createEditautomaton1Group());
	}

	/**
	* Creates "editautomaton" palette tool group
	* @generated
	*/
	private PaletteContainer createEditautomaton1Group() {
		PaletteGroup paletteContainer = new PaletteGroup(Messages.Editautomaton1Group_title);
		paletteContainer.setId("createEditautomaton1Group"); //$NON-NLS-1$
		paletteContainer.add(createTransition1CreationTool());
		paletteContainer.add(createInitialState2CreationTool());
		paletteContainer.add(createRegularState3CreationTool());
		return paletteContainer;
	}

	/**
	* @generated
	*/
	private ToolEntry createTransition1CreationTool() {
		DefaultLinkToolEntry entry = new DefaultLinkToolEntry(Messages.Transition1CreationTool_title,
				Messages.Transition1CreationTool_desc,
				Collections.singletonList(EditautomatonElementTypes.Transition_4001));
		entry.setId("createTransition1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(EditautomatonElementTypes.getImageDescriptor(EditautomatonElementTypes.Transition_4001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createInitialState2CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.InitialState2CreationTool_title,
				Messages.InitialState2CreationTool_desc,
				Collections.singletonList(EditautomatonElementTypes.InitialState_2001));
		entry.setId("createInitialState2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(EditautomatonElementTypes.getImageDescriptor(EditautomatonElementTypes.InitialState_2001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	* @generated
	*/
	private ToolEntry createRegularState3CreationTool() {
		DefaultNodeToolEntry entry = new DefaultNodeToolEntry(Messages.RegularState3CreationTool_title,
				Messages.RegularState3CreationTool_desc,
				Collections.singletonList(EditautomatonElementTypes.RegularState_2002));
		entry.setId("createRegularState3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(EditautomatonElementTypes.getImageDescriptor(EditautomatonElementTypes.RegularState_2002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

}
