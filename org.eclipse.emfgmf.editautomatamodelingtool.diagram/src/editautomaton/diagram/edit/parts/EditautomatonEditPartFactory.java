package editautomaton.diagram.edit.parts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.directedit.locator.CellEditorLocatorAccess;

import editautomaton.diagram.part.EditautomatonVisualIDRegistry;

/**
 * @generated
 */
public class EditautomatonEditPartFactory implements EditPartFactory {

	/**
	* @generated
	*/
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof View) {
			View view = (View) model;
			switch (EditautomatonVisualIDRegistry.getVisualID(view)) {

			case EditAutomatonEditPart.VISUAL_ID:
				return new EditAutomatonEditPart(view);

			case InitialStateEditPart.VISUAL_ID:
				return new InitialStateEditPart(view);

			case InitialStateNameEditPart.VISUAL_ID:
				return new InitialStateNameEditPart(view);

			case RegularStateEditPart.VISUAL_ID:
				return new RegularStateEditPart(view);

			case RegularStateNameEditPart.VISUAL_ID:
				return new RegularStateNameEditPart(view);

			case TransitionEditPart.VISUAL_ID:
				return new TransitionEditPart(view);

			case TransitionInterceptedActionEditPart.VISUAL_ID:
				return new TransitionInterceptedActionEditPart(view);

			case TransitionActionToPerformEditPart.VISUAL_ID:
				return new TransitionActionToPerformEditPart(view);

			}
		}
		return createUnrecognizedEditPart(context, model);
	}

	/**
	* @generated
	*/
	private EditPart createUnrecognizedEditPart(EditPart context, Object model) {
		// Handle creation of unrecognized child node EditParts here
		return null;
	}

	/**
	* @generated
	*/
	public static CellEditorLocator getTextCellEditorLocator(ITextAwareEditPart source) {
		return CellEditorLocatorAccess.INSTANCE.getTextCellEditorLocator(source);
	}

}
