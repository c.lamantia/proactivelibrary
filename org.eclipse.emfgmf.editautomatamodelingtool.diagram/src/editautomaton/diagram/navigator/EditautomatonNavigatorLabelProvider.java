package editautomaton.diagram.navigator;

import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserOptions;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;

import editautomaton.EditAutomaton;
import editautomaton.diagram.edit.parts.EditAutomatonEditPart;
import editautomaton.diagram.edit.parts.InitialStateEditPart;
import editautomaton.diagram.edit.parts.InitialStateNameEditPart;
import editautomaton.diagram.edit.parts.RegularStateEditPart;
import editautomaton.diagram.edit.parts.RegularStateNameEditPart;
import editautomaton.diagram.edit.parts.TransitionEditPart;
import editautomaton.diagram.edit.parts.TransitionInterceptedActionEditPart;
import editautomaton.diagram.part.EditautomatonDiagramEditorPlugin;
import editautomaton.diagram.part.EditautomatonVisualIDRegistry;
import editautomaton.diagram.providers.EditautomatonElementTypes;
import editautomaton.diagram.providers.EditautomatonParserProvider;

/**
 * @generated
 */
public class EditautomatonNavigatorLabelProvider extends LabelProvider
		implements ICommonLabelProvider, ITreePathLabelProvider {

	/**
	* @generated
	*/
	static {
		EditautomatonDiagramEditorPlugin.getInstance().getImageRegistry().put("Navigator?UnknownElement", //$NON-NLS-1$
				ImageDescriptor.getMissingImageDescriptor());
		EditautomatonDiagramEditorPlugin.getInstance().getImageRegistry().put("Navigator?ImageNotFound", //$NON-NLS-1$
				ImageDescriptor.getMissingImageDescriptor());
	}

	/**
	* @generated
	*/
	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		Object element = elementPath.getLastSegment();
		if (element instanceof EditautomatonNavigatorItem
				&& !isOwnView(((EditautomatonNavigatorItem) element).getView())) {
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	* @generated
	*/
	public Image getImage(Object element) {
		if (element instanceof EditautomatonNavigatorGroup) {
			EditautomatonNavigatorGroup group = (EditautomatonNavigatorGroup) element;
			return EditautomatonDiagramEditorPlugin.getInstance().getBundledImage(group.getIcon());
		}

		if (element instanceof EditautomatonNavigatorItem) {
			EditautomatonNavigatorItem navigatorItem = (EditautomatonNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		return super.getImage(element);
	}

	/**
	* @generated
	*/
	public Image getImage(View view) {
		switch (EditautomatonVisualIDRegistry.getVisualID(view)) {
		case EditAutomatonEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Diagram?http://org.eclipse.emfgmf.editautomatamodelingtool/model/editautomaton.ecore?EditAutomaton", //$NON-NLS-1$
					EditautomatonElementTypes.EditAutomaton_1000);
		case InitialStateEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://org.eclipse.emfgmf.editautomatamodelingtool/model/editautomaton.ecore?InitialState", //$NON-NLS-1$
					EditautomatonElementTypes.InitialState_2001);
		case RegularStateEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://org.eclipse.emfgmf.editautomatamodelingtool/model/editautomaton.ecore?RegularState", //$NON-NLS-1$
					EditautomatonElementTypes.RegularState_2002);
		case TransitionEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://org.eclipse.emfgmf.editautomatamodelingtool/model/editautomaton.ecore?Transition", //$NON-NLS-1$
					EditautomatonElementTypes.Transition_4001);
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	* @generated
	*/
	private Image getImage(String key, IElementType elementType) {
		ImageRegistry imageRegistry = EditautomatonDiagramEditorPlugin.getInstance().getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null && elementType != null && EditautomatonElementTypes.isKnownElementType(elementType)) {
			image = EditautomatonElementTypes.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null) {
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	* @generated
	*/
	public String getText(Object element) {
		if (element instanceof EditautomatonNavigatorGroup) {
			EditautomatonNavigatorGroup group = (EditautomatonNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof EditautomatonNavigatorItem) {
			EditautomatonNavigatorItem navigatorItem = (EditautomatonNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return null;
			}
			return getText(navigatorItem.getView());
		}

		return super.getText(element);
	}

	/**
	* @generated
	*/
	public String getText(View view) {
		if (view.getElement() != null && view.getElement().eIsProxy()) {
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (EditautomatonVisualIDRegistry.getVisualID(view)) {
		case EditAutomatonEditPart.VISUAL_ID:
			return getEditAutomaton_1000Text(view);
		case InitialStateEditPart.VISUAL_ID:
			return getInitialState_2001Text(view);
		case RegularStateEditPart.VISUAL_ID:
			return getRegularState_2002Text(view);
		case TransitionEditPart.VISUAL_ID:
			return getTransition_4001Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	* @generated
	*/
	private String getEditAutomaton_1000Text(View view) {
		EditAutomaton domainModelElement = (EditAutomaton) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			EditautomatonDiagramEditorPlugin.getInstance()
					.logError("No domain element for view with visualID = " + 1000); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getInitialState_2001Text(View view) {
		IParser parser = EditautomatonParserProvider.getParser(EditautomatonElementTypes.InitialState_2001,
				view.getElement() != null ? view.getElement() : view,
				EditautomatonVisualIDRegistry.getType(InitialStateNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			EditautomatonDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getRegularState_2002Text(View view) {
		IParser parser = EditautomatonParserProvider.getParser(EditautomatonElementTypes.RegularState_2002,
				view.getElement() != null ? view.getElement() : view,
				EditautomatonVisualIDRegistry.getType(RegularStateNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			EditautomatonDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 5002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getTransition_4001Text(View view) {
		IParser parser = EditautomatonParserProvider.getParser(EditautomatonElementTypes.Transition_4001,
				view.getElement() != null ? view.getElement() : view,
				EditautomatonVisualIDRegistry.getType(TransitionInterceptedActionEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			EditautomatonDiagramEditorPlugin.getInstance().logError("Parser was not found for label " + 6001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	* @generated
	*/
	private String getUnknownElementText(View view) {
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	* @generated
	*/
	private String getUnresolvedDomainElementProxyText(View view) {
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	* @generated
	*/
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	* @generated
	*/
	public void restoreState(IMemento aMemento) {
	}

	/**
	* @generated
	*/
	public void saveState(IMemento aMemento) {
	}

	/**
	* @generated
	*/
	public String getDescription(Object anElement) {
		return null;
	}

	/**
	* @generated
	*/
	private boolean isOwnView(View view) {
		return EditAutomatonEditPart.MODEL_ID.equals(EditautomatonVisualIDRegistry.getModelID(view));
	}

}
