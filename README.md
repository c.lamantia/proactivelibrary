# ProactiveLibrary

The ProactiveLibraryCodeGeneration project is organized in EMEditor and PMGenerator.

The whole project has been implemented in the Eclipse environment augmented by the EMF and GMF plugins for the creation of the automata editor and by Acceleo for the automatic code generation.

## EMEditor 
The folders org.eclipse.emfgmf.editautomatamodelingtool is the Eclipse project to be imported for the execution of the automaton editor. The details regarding the development of the Eclipse environment and the installation of the necessary plugins are specified in the README that can be found at the folder org.eclipse.emfgmf.editautomatamodelingtool

## PMGenerator
The folder org.eclipse.acceleo.policyenforcergenerator contains the Eclipse project that allows the automatic generation of code from an automaton defined in the editor. The details related to the installation of plugins and Eclipse environment setups are described it the README available inside that folder.