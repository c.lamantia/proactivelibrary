/**
 */
package editautomaton.tests;

import editautomaton.EditAutomaton;
import editautomaton.EditautomatonFactory;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Edit Automaton</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class EditAutomatonTest extends TestCase {

	/**
	 * The fixture for this Edit Automaton test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EditAutomaton fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(EditAutomatonTest.class);
	}

	/**
	 * Constructs a new Edit Automaton test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EditAutomatonTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Edit Automaton test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(EditAutomaton fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Edit Automaton test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EditAutomaton getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(EditautomatonFactory.eINSTANCE.createEditAutomaton());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //EditAutomatonTest
